/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_session_8;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;

/**
 *
 * @author Dev
 */
public class MyInt implements Comparable {
    
    private int _value;

    public MyInt(int value) {
        this._value = value;
    }

    public int getValue() {
        return _value;
    }

    public void setValue(int value) {
        this._value = value;
    }
    
    public int compare(Comparable otherItem) {
        if(_value > ((MyInt)otherItem)._value){
            return 1;
        }else if(_value == ((MyInt)otherItem)._value){
            return 0;
        }else{
            return -1;
        }
    } 

    @Override
    public String toString() {
        return "MyInt{" + "_value=" + _value + '}';
    }    
}
