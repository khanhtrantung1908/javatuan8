/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_session_8;

/**
 *
 * @author Dev
 */
public interface BaseInterface {
    public int getId();

    public void setId(int id);

    public String getName();

    public void setName(String name);
}
