/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_session_8;

/**
 *
 * @author Dev
 */
public class Student implements BaseInterface{    
    private int _id;
    
    private String _name;

    public int getId() {
        return _id;
    }

    @Override
    public String toString() {
        return "Student{" + "_id=" + _id + ", _name=" + _name + '}';
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }
    
    
}
