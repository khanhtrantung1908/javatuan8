/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_session_8;

/**
 *
 * @author Dev
 */
public class Node<T extends Comparable> {
    public T Item;

    public Node(T item, Node next) {
        this.Item = item;
        this.Next = next;
    }
    
    public Node Next;
}
