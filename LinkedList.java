package java_session_8;

public class LinkedList<T extends Comparable> {
    protected Node<T> firstNode;
  
    public LinkedList(){
        firstNode = null;
    }
    
    public boolean find(T item) {
        Node<T> index = firstNode;
        while (index != null) {
            if(index.Item == item){
                return true;
            }            
        }
        return false;
    }
    
    public void insert(T item) {
        Node<T> node = new Node<T>(item, firstNode);
        firstNode = node;
    }
    
    public LinkedList<T> sortedList(){
        int typeCompare = 1;
        
        //
        LinkedList<T> res = new LinkedList<T>();
        
        Node<T> index = firstNode;
        
        while (index != null) {
            if(res.firstNode == null){
                T max = index.Item;
                Node<T> index2 = firstNode;
                while (index2 != null) {
                    if(max.compare(index2.Item) == typeCompare){
                        max = index2.Item;
                    }
                    index2 = index2.Next;  
                }
                res.firstNode = new Node<T>(max, null);
            }else{
                Node<T> resIndex = res.firstNode;
                while (resIndex.Next != null && index.Item.compare(resIndex.Next.Item) == typeCompare) {
                    resIndex = resIndex.Next;                    
                }
                if(resIndex.Next == null)
                    resIndex.Next = new Node(index.Item, null);
                else{
                    // nut hien tai nho hon nut them
                    resIndex.Next = new Node(index.Item, resIndex.Next);
                }
            }
            index = index.Next;
        }       
        
        
        
        return res;
    }
    
    public void print(){
        if(firstNode != null){
            Node index = firstNode;
            
            System.out.println("-------------------------");
            while (index != null) {
                System.out.println(index.Item.toString());
                index = index.Next;
            }      
            System.out.println("-------------------------");      
        }
    }
}

