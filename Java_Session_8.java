/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_session_8;

/**
 *
 * @author Dev
 */
public class Java_Session_8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LinkedList<MyInt> list = new LinkedList<>();
        list.insert(new MyInt(1));
        list.insert(new MyInt(2));
        list.insert(new MyInt(3));
        list.insert(new MyInt(4));
        list.insert(new MyInt(3));
        list.insert(new MyInt(2));
        list.insert(new MyInt(1));
        list.print();
        LinkedList<MyInt> list2 = list.sortedList();
        list2.print();
    }
    
    public static <TFrom extends BaseInterface, TTo extends BaseInterface> void mySwap(TFrom from, TTo to){
        int id = from.getId();
        String name = from.getName();
        
        from.setId(to.getId());
        from.setName(to.getName());
        
        to.setId(id);
        to.setName(name);        
    }
    
    public static void TestMySwap(){
        Student student = new Student();
        student.setId(1);
        student.setName("Student");
        Company company = new Company();
        company.setId(2);
        company.setName("Company");
        
        System.out.println(student.toString());
        System.out.println(company.toString());
        mySwap(student, company);
        System.out.println(student.toString());
        System.out.println(company.toString());        
    }
    
}


